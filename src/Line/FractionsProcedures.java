package Line;

/**
 * Класс операций с дробями, содержит методы:
 * -frDivide(Деление дробей);
 * -frMultiply(Умножение дробей);
 * -frAdd(Сложение дробей);
 * -frSubtract(Вычитание дробей);
 * toCommonDenominator(Метод приведения к общему знаменателю);
 * getCommonDenominator(Метод для получения общего знаменателя);
 * reduce(Сокращение дроби).
 *
 * @author Isheykin A.A.
 */
public class FractionsProcedures {
    // Деление
    public static Fraction frDivide(Fraction fFr, Fraction sFr) {
        return new Fraction(fFr.getNominator()*sFr.getDenominator(),
                            fFr.getDenominator()*sFr.getNominator());
    }

    // Умножение
    // todo: найдена ошибка в методе frMultiply
    public static Fraction frMultiply(Fraction fFr, Fraction sFr) {
        return new Fraction(fFr.getNominator()*sFr.getNominator(),
                            fFr.getDenominator()*sFr.getDenominator());
    }

    // Сложение
    public static Fraction frAdd(Fraction fFr, Fraction sFr) {
        toCommonDenominator(fFr, sFr);
        return new Fraction(fFr.getNominator()+sFr.getNominator(),
                            fFr.getDenominator());
    }

    // Вычитание
    public static Fraction frSubstract(Fraction fFr, Fraction sFr) {
        toCommonDenominator(fFr, sFr);
        return new Fraction(fFr.getNominator()-sFr.getNominator(),
                            fFr.getDenominator());
    }

    // Метод приведения дробей к общему знаменателю
    public static void toCommonDenominator(Fraction fFr, Fraction sFr) {
        if (fFr.getDenominator()!=sFr.getDenominator()) {
            fFr.setNominator(fFr.getNominator()*sFr.getDenominator());
            sFr.setNominator(sFr.getNominator()*fFr.getDenominator());
            int commonDenominator = getCommonDenominator(fFr, sFr);
            fFr.setDenominator(commonDenominator);
            sFr.setDenominator(commonDenominator);
        }
    }

    // Метод для поиска общего знаменателя
    public static int getCommonDenominator(Fraction fFr, Fraction sFr) {
        // Самая легкая реализация(?)
        int commonDenominator = fFr.getDenominator() * sFr.getDenominator();
        return commonDenominator;
    }

    // Метод для сокращения дроби
    public static Fraction reduce(Fraction fraction) {
        for (int i = 2; i<fraction.getDenominator(); i++) {
            while (fraction.getNominator() % i == 0 && fraction.getDenominator() % i == 0) {
                fraction.setNominator(fraction.getNominator() / i);
                fraction.setDenominator(fraction.getDenominator() / i);
            }
                if (fraction.getNominator()%i!=0 | fraction.getDenominator()%i!=0) {
                    continue;
                }
                return new Fraction(fraction.getNominator(), fraction.getDenominator());
            }
        return fraction;
    }

    /**
     * Метод для перевода строки, содержащую дробь в Fraction;
     * В начале происходит проверка, какая строка была передана в пареметры:
     * 1) Строка, содержащая недесятичную дробь
     * - Происходит считывание символов дo '/'(числитель) и после '/'(знаменатель);
     * - Преобразование считанных строк в числовой тип данных;
     * - Возврат полученной дроби;
     * 2) Строка, содержащая десятичную дробь:
     * - Происходит считывание символов дo '.'(числитель) и после '.'(дробная часть);
     * - Преобразование считанных строк в числовой тип данных;
     * - Возврат полученной дроби;
     * 3) Строка, содержащая целое число:
     * - Возврат дроби с заданным строке значением целой части и знаменателем '1'.
     * @param strFraction
     * @return new Fraction(nominator, denominator)
     */
    public static Fraction convert(String strFraction) {
        int nominator;
        int denominator;
        String strNominator = "";
        String strDenominator = "";
        if (strFraction.contains("/")) {
            for (int i = strFraction.indexOf('/') - 1; i!=-1; i--) {
                strNominator = strFraction.charAt(i) + strNominator;
            }
            nominator=Integer.parseInt(strNominator);
            for (int y = strFraction.indexOf('/') + 1; y!=strFraction.length(); y++) {
                strDenominator = strDenominator + strFraction.charAt(y);
            }
            denominator=Integer.parseInt(strDenominator);
            return new Fraction(nominator, denominator);
        }
        if (strFraction.contains(".")) {
            for (int x = strFraction.indexOf('.'); x<strFraction.length(); x++) {
                strNominator = strNominator + strFraction.charAt(x);
            }
            nominator=Integer.parseInt(strNominator);
            for (int l = strFraction.indexOf('.'); l<=0; l--) {
                strDenominator = strDenominator + strFraction.charAt(l);
            }
            denominator=Integer.parseInt(strDenominator);
            return new Fraction(nominator, denominator);
        } else {
            return new Fraction(Integer.parseInt(strFraction), 1);
        }

    }

    /**
     * Метод для перенаправления
     * в необходимые методы для совершения подсчетов
     * в зависимости от знака в выражении
     * @param fFr
     * @param sFr
     * @param sign
     * @return Fraction result(результат вычислений).
     */
    public static Fraction calculate(Fraction fFr, Fraction sFr, char sign) {
        Fraction result=new Fraction(1,1);
        // костыль для инициализации результата до вычислений
        switch (sign) {
            case ':' :
                result=frDivide(fFr, sFr);
                return result;
            case '+' :
                result=frAdd(fFr, sFr);
                return result;
            case '-' :
                result=frSubstract(fFr, sFr);
                return result;
            case '*' :
                result=frMultiply(fFr, sFr);
        }
        return reduce(result);
    }
}
