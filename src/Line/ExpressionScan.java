/**
 *
 */


package Line;

import java.util.Scanner;


public class ExpressionScan {
    private static Scanner scanner = new Scanner(System.in);
    public static int[] numberSequence = {0,1,2,3,4,5,6,7,8,9};

    public static String input() {
        System.out.println("Введите выражение\nСправка:\nДроби в выражении пишутся по типу:\n (числитель)/(знаменатель)");
        return scanner.nextLine();
    }

    public static void scanExpression(String expression_string) {
        Expression expression = new Expression();
        expression.setExpressionSign(ExpressionScan.signScan(expression_string));
        int positionSign = ExpressionScan.searchPositionSign(expression_string);
        String firstFraction = (ExpressionScan.scanFirstNumber(expression_string, positionSign));
        expression.setFirstFraction(FractionsProcedures.convert(firstFraction));
        String secondFraction = (ExpressionScan.scanSecondNumber(expression_string, positionSign));
        expression.setSecondFraction(FractionsProcedures.convert(secondFraction));
        ExpressionScan.calculateExpression(expression);
        ExpressionScan.substringExpression(expression, expression_string);
    }

    public static char signScan(String expression) {
        String signs = "*:+-";
        char searchableSign;
        for (int signNum = 0; signNum < expression.length(); signNum++) {
            for (int signNumber = 0; signNumber < signs.length(); signNumber++) {
                searchableSign = signs.charAt(signNumber);
                if (expression.charAt(signNum) == searchableSign) {
                    return expression.charAt(signNum);
                } else continue;
            }
        }
        return '?';
    }

    public static int searchPositionSign(String expression) {
        String signs = "*:+-";
        char searchableSign;
        for (int signNum = 0; signNum < expression.length(); signNum++) {
            for (int signNumber = 0; signNumber < signs.length(); signNumber++) {
                searchableSign = signs.charAt(signNumber);
                if (expression.charAt(signNum) == searchableSign) {
                    return signNum;
                } else continue;
            }
        }
        return 0;
    }


    public static void calculateExpression(Expression expression) {
        Fraction result = FractionsProcedures.calculate(expression.getFirstFraction(),
                                                expression.getSecondFraction(),
                                                expression.getExpressionSign());
        System.out.println(result);
    }

    public static void substringExpression(Expression expression, String expression_string) {
        // StringBuffer stringBuffer = new StringBuffer();
        // stringBuffer.delete(expression.beginIndex, expression.endIndex);
        // System.out.println(expression);
    }


    public static String scanFirstNumber(String expression_string, int signNum) {
        String firstNumber = "";
        boolean isExpressionEnd = false;
        while (isExpressionEnd == false) {
            for (int signNumber = signNum-1; isExpressionEnd == false; signNumber--) {
                if (expression_string.charAt(signNumber) == '1' ||
                        expression_string.charAt(signNumber) == '2' ||
                        expression_string.charAt(signNumber) == '3' ||
                        expression_string.charAt(signNumber) == '4' ||
                        expression_string.charAt(signNumber) == '5' ||
                        expression_string.charAt(signNumber) == '6' ||
                        expression_string.charAt(signNumber) == '7' ||
                        expression_string.charAt(signNumber) == '8' ||
                        expression_string.charAt(signNumber) == '9' ||
                        expression_string.charAt(signNumber) == '0' ||
                        expression_string.charAt(signNumber) == ',' ||
                        expression_string.charAt(signNumber) == '/') {
                    firstNumber = expression_string.charAt(signNumber) + firstNumber;
                    if (signNumber == 0) {
                        isExpressionEnd = true;
                        break;
                    }
                } else {
                    isExpressionEnd = true;
                    break;
                }
            }
        }
        return firstNumber;
    }

    public static String scanSecondNumber(String expression_string, int signNum) {
        String secondNumber = "";
        boolean isExpressionEnd = false;
        while (isExpressionEnd == false) {
            for (int signNumber = signNum+1; isExpressionEnd == false; signNumber++) {
                if (expression_string.charAt(signNumber) == '1' ||
                        expression_string.charAt(signNumber) == '2' ||
                        expression_string.charAt(signNumber) == '3' ||
                        expression_string.charAt(signNumber) == '4' ||
                        expression_string.charAt(signNumber) == '5' ||
                        expression_string.charAt(signNumber) == '6' ||
                        expression_string.charAt(signNumber) == '7' ||
                        expression_string.charAt(signNumber) == '8' ||
                        expression_string.charAt(signNumber) == '9' ||
                        expression_string.charAt(signNumber) == '0' ||
                        expression_string.charAt(signNumber) == ',' ||
                        expression_string.charAt(signNumber) == '/') {
                    secondNumber = secondNumber + expression_string.charAt(signNumber);
                    if (signNumber == expression_string.length()-1) {
                        isExpressionEnd = true;
                        break;
                    }
                } else {
                    isExpressionEnd = true;
                    break;
                }
            }
        }
        return secondNumber;
    }
}

