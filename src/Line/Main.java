package Line;

/**
 *
 * Main класс, выполняет вычисление выражения, содержащего дроби.
 * Результат программы записывается в текстовый файл
 * Демонстрирует работу классов:
 * - Expression;
 * - ExpressionScan;
 * - Fraction;
 * - FractionProcedures;
 * @author Isheykin A.A.
 */
public class Main {
    public static String expression_string = ExpressionScan.input();
    public static void main(String[] args) {
        ExpressionScan.scanExpression(expression_string);
    }
}
