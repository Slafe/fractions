package Line;

/**
 *
 */
public class Fraction {
    private int nominator;
    private int denominator;

    public Fraction(int nominator, int denominator) {
        this.nominator = nominator;
        this.denominator = denominator;
    }

    public Fraction() {
        this(1,1);
    }

    public void setNominator(int nominator) {
        this.nominator = nominator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    @Override
    public java.lang.String toString() {
        return  nominator + "/" + denominator;
    }
}
