package Line;

import java.lang.String;

/**
 * Класс, характеризующий объект "выражение"(Expression)
 */
public class Expression {
    //---Fields---
    public Fraction firstFraction;
    public Fraction secondFraction;
    public char expressionSign;
    //------------


    public Expression(char expressionSign, Fraction firstFraction, Fraction secondFraction) {
        this.expressionSign = expressionSign;
        this.firstFraction = firstFraction;
        this.secondFraction = secondFraction;
    }

    public Expression() {
        this.expressionSign='-';
        this.firstFraction = new Fraction();
        this.secondFraction = new Fraction();
    }

    public Fraction getFirstFraction() {
        return firstFraction;
    }

    public void setFirstFraction(Fraction firstFraction) {
        this.firstFraction = firstFraction;
    }

    public Fraction getSecondFraction() {
        return secondFraction;
    }

    public void setSecondFraction(Fraction secondFraction) {
        this.secondFraction = secondFraction;
    }

    public char getExpressionSign() {
        return expressionSign;
    }

    public void setExpressionSign(char expressionSign) {
        this.expressionSign = expressionSign;
    }

    @Override
    public String toString() {
        return firstFraction.toString() +
                expressionSign +
                secondFraction.toString();
    }
}
